import { warn, fail } from "danger";

//fail("above warn")
warn("A warn message")
//fail("below warn")

if (results?.warnings?.length > 0 && (results?.errors || []).length === 0) {
  process.exit(2)
}
